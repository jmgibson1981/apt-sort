#Copyright <2023> <Jason Gibson>

#<BSD License 2.0>

#Redistribution and use in source and binary forms,
#with or without modification, are permitted provided
#that the following conditions are met:

#1. Redistributions of source code must retain the above
#copyright notice, this list of conditions and the
#following disclaimer.

#2. Redistributions in binary form must reproduce the
#above copyright notice, this list of conditions and
#the following disclaimer in the documentation and/or
#other materials provided with the distribution.

#3. Neither the name of the copyright
#holder nor the names of its contributors may be used
#to endorse or promote products derived from this
#software without specific prior written permission.

#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS
#AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
#WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
#THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
#DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
#CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
#USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
#HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
#IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
#OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
#THE POSSIBILITY OF SUCH DAMAGE.

# default target

.DEFAULT_GOAL := build

# variables
CC = gcc
CFLAGS = -O2
TESTCFLAGS = -Wredundant-decls -Wfloat-equal -Winline -Wunreachable-code -pedantic -Wextra -Wall -g

# files to work with
FILELIST = main.c args.c print.c
OUTPUTBIN = aptsort
INSTDIR = /usr/local/bin

# external libraries to link
# requires library from https://gitlab.com/jmgibson1981/mylibraries
NIXLIBLINK = -ljmgeneral

# target locations and file names
TAR = ${INSTDIR}

build: # executable build
	@clear
	@echo
	@echo "compiled for normal use. no debug options"
	@${CC} ${FILELIST} ${NIXLIBLINK} ${CFLAGS} -o ${OUTPUTBIN}
	@echo "executable built"
	@echo

install: # install to target
	@clear
	@echo
	@if [ -f ${TAR}/${OUTPUTBIN} ] ; then rm ${TAR}/${OUTPUTBIN} ; fi
	@if [ ! -d ${INSTDIR} ] ; then install -d ${INSTDIR} ; chmod 775 ${INSTDIR} ; fi
	@install -m 755 ${OUTPUTBIN} ${TAR}/ ; echo "installed executable file"
	@rm ${OUTPUTBIN}
	@echo

test: # test build mode with all debug options
	@clear
	@echo
	@echo "compiled in test mode. debug options enabled"
	@${CC} ${FILELIST} ${NIXLIBLINK} ${TESTCFLAGS} -o ${OUTPUTBIN}.test
	@echo

uninstall: # uninstall from target
	@clear
	@echo
	@if [ -f ${TAR}/${OUTPUTBIN} ] ; then rm ${TAR}/${OUTPUTBIN} ; fi
	@echo
	@echo "installed files removed"
	@echo

clean: # eliminate stray files created during build process / testing files to clean directory
	@clear
	@if [ -f ${OUTPUTBIN} ] ; then rm ${OUTPUTBIN} ; fi
	@if [ -f ${OUTPUTBIN}.test ] ; then rm ${OUTPUTBIN}.test ; fi
	@echo
	@echo "directory cleaned for next compilation"
	@echo
