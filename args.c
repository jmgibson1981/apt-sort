/*
Copyright <2024> <Jason Gibson>

<BSD License 2.0>

Redistribution and use in source and binary forms,
with or without modification, are permitted provided
that the following conditions are met:

1. Redistributions of source code must retain the above
copyright notice, this list of conditions and the
following disclaimer.

2. Redistributions in binary form must reproduce the
above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright
holder nor the names of its contributors may be used
to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS
AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <jmgeneral.h>
#include "args.h"
#include "print.h"

// declare local functions
static void
test_formatting(char *teststring,
                REGCHECKS *t1);

static int
get_linux_version(REGCHECKS *pt1);

// return struct based on args
REGCHECKS
arg_process(int argc,
            char ** args)
{
   // declare struct and initialize
   REGCHECKS t1;
   init_regchecks_struct(&t1,
                         false,
                         false);

   // load linux version into struct
   // fail if os-release file not found
   if (get_linux_version(&t1) != 0) {
      init_regchecks_struct(&t1,
                            true,
                            true);
      t1.bailout = true;

      return(t1);
   };

   if (argc == 2) {
      test_formatting(args[1],
                      &t1);
   } else {
      int option = 0;
      int optcounter = 1;
      while ((option = getopt(argc,
                              args, 
                              "a:p:")) != -1) {
         if (t1.bailout) {
            break;
         }
         switch (option) {
            case 'a':
               if (args[optcounter + 1]) {
                  t1.architecture = strdup(args[optcounter + 1]);
                  optcounter += 2;
               } else {
                  init_regchecks_struct(&t1,
                                       true,
                                       true);
               }
               break;
            case 'p':
               if (args[optcounter + 1]) {
                  test_formatting(args[optcounter + 1],
                                 &t1);
                  optcounter += 2;
               } else {
                  init_regchecks_struct(&t1,
                                       true,
                                       true);
               }
               break;
            default:
               init_regchecks_struct(&t1,
                                    true,
                                    true);
               break;
         }
      }
   }

   return(t1);
}

static void
test_formatting(char *teststring,
                REGCHECKS *t1)
{
   if (teststring[0] == '^') {
      // start of string regex
      t1->startofstring = true;
      t1->retval = string_field_search(teststring,
                                       "^",
                                       0);
   } else if (teststring[strlen(teststring) - 1] == '$') {
      // end of string regex
      t1->endofstring = true;
      t1->retval = string_field_search(teststring,
                                       "$",
                                       0);
   } else {
      // no regex symbols
      t1->retval = strdup(teststring);
   }
}

void
init_regchecks_struct(REGCHECKS *pt1,
                      bool breakout,
                      bool cleanup)
{
   // initialize with false
   // clean up struct for end of program / errors via bool
   pt1->startofstring = false;
   pt1->endofstring = false;
   pt1->print = false;
   pt1->bailout = breakout;
   pt1->packagecount = 0;
   if (cleanup) {
      free(pt1->retval);
      free(pt1->architecture);
      free(pt1->linuxversion);
   }
   pt1->retval = NULL;
   pt1->architecture = NULL;
   pt1->linuxversion = NULL;
}

static int
get_linux_version(REGCHECKS *pt1)
{
   // local initializations
   int retval = 0;
   char *buffer = NULL;
   FILE *osrelease = fopen(OSRELEASE,
                           "r");
   
   // set start point to double back if first pattern not found
   fpos_t a;
   fgetpos(osrelease, &a);



   if (osrelease) {
      // find ID string in file and set version in struct
      while (!feof(osrelease)) {
         free(buffer); buffer = NULL;
         buffer = getline_mem_alloc(osrelease);
         if ((buffer[0] == 'I' &&
             buffer[1] == 'D') &&
             (buffer[2] == '=' ||
              buffer[2] == '_')) {
            if (strstr(buffer, "debian") ||
                strstr(buffer, "ubuntu")) {
               pt1->linuxversion = strdup("debian");
               break;
            }
         }
      }
      free(buffer); buffer = NULL;
      fclose(osrelease); osrelease = NULL;
   } else {
      osrelease = NULL;
      retval = -1;
   }
   return(retval);
}

void print_wrong_args(void)
{
   fprintf(stderr,
           "arguments incorrect. check and try again\n");
}
