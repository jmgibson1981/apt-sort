/*
Copyright <2024> <Jason Gibson>

<BSD License 2.0>

Redistribution and use in source and binary forms,
with or without modification, are permitted provided
that the following conditions are met:

1. Redistributions of source code must retain the above
copyright notice, this list of conditions and the
following disclaimer.

2. Redistributions in binary form must reproduce the
above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright
holder nor the names of its contributors may be used
to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS
AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
THE POSSIBILITY OF SUCH DAMAGE.
*/

/*
 * Version 0.5
 *
 * Ridiculous program to make apt searching a single command
 * over a chain of nix commands. Wrote this to make it a
 * single command to find doc packages
 *
 * REQUIRES:
 * https://gitlab.com/jmgibson1981/mylibraries
 *
 * TODO: add other distro support
 *
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <jmgeneral.h>
#include "args.h"
#include "print.h"

int main(int argc, char ** argv)
{
   long int retval = 0;
   // verify args and bail if something incorrect
   if (argc < 2) {
      print_wrong_args();
      return(-1);
   }

   // validate args and create list. bail as needed
   REGCHECKS t1 = arg_process(argc,
                              argv);
   if (t1.bailout == true) {
      print_wrong_args();
      return(-2);
   }

   // fire proper function depending on distro
   if (t1.retval) {
      if (strcmp(t1.linuxversion,
                 "debian") == 0) {
         print_debian(&t1);
      }
   } else {
      init_regchecks_struct(&t1,
                            false,
                            true);
      print_wrong_args();

      return(-3);

   }

   if (t1.packagecount == 0) {
      printf("\nno packages match. verify your arguments\n\n");
   } else {
      retval = t1.packagecount;
   }

   init_regchecks_struct(&t1,
                         false,
                         true);
   return(retval);
}
