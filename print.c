/*
Copyright <2024> <Jason Gibson>

<BSD License 2.0>

Redistribution and use in source and binary forms,
with or without modification, are permitted provided
that the following conditions are met:

1. Redistributions of source code must retain the above
copyright notice, this list of conditions and the
following disclaimer.

2. Redistributions in binary form must reproduce the
above copyright notice, this list of conditions and
the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright
holder nor the names of its contributors may be used
to endorse or promote products derived from this
software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS
AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <dirent.h>
#include <jmgeneral.h>
#include "apt-sort.h"

#define PACKLISTPATH "/var/lib/apt/lists/"

void print_debian(REGCHECKS *pt1)
{
   DIR *dir = opendir(PACKLISTPATH);

   FILE * filetoread = NULL;
   struct dirent *eachfile;
   char * filebuffer = NULL;
   char * filereadbuffer = NULL;
   char * printbuffer = NULL;
   char * localbuf = NULL;
   size_t pcounter = 0;

   while ((eachfile = readdir(dir)) != NULL) {
      // confirm valid file to read via naming scheme
      if (strstr(eachfile->d_name,
                 "Packages") &&
          !strstr(eachfile->d_name,
                  "Index") &&
          !strstr(eachfile->d_name,
                  "Package:")) {
         if (pt1->architecture) {
            // skip if architecture specified and file not match architecture
            if (!strstr(eachfile->d_name,
                        pt1->architecture)) {
               continue;
            }
         }
         filebuffer = strdup(PACKLISTPATH);
         size_t len = strlen(filebuffer) +
                      strlen(eachfile->d_name);
         filebuffer = realloc(filebuffer,
                              len +
                              1);
         strncat(filebuffer, 
                 eachfile->d_name,
                 len);
         filetoread = fopen(filebuffer,
                            "r");
         free(filebuffer); filebuffer = NULL;
         while (!feof(filetoread)) {
            filereadbuffer = getline_mem_alloc(filetoread);
            if (filereadbuffer) {
               localbuf = string_field_search(filereadbuffer,
                                                     " ",
                                                     0);
               if (localbuf) {
                  // verify string contents before running print operation
                  if (strcmp(localbuf,
                             "Package:") == 0) {
                  printbuffer = string_field_search(filereadbuffer,
                                                      " ",
                                                      1);
                     if (strstr(printbuffer,
                                pt1->retval)) {
                        size_t localcounter = 0;
                        size_t retvallen = strlen(pt1->retval);
                        if (pt1->startofstring ||
                            pt1->endofstring) {
                           // reverse strings to test contents for end of string
                           // regex
                           if (pt1->endofstring)  {
                              string_flipper(printbuffer);
                              string_flipper(pt1->retval);
                           }
                           for (size_t i = 0; i < retvallen; i++) {
                              if(pt1->retval[i] == printbuffer[i]) {
                                 localcounter++;
                              }
                           }
                           // flip string to proper direction once done
                           // end of string regex
                           if (pt1->endofstring) {
                              string_flipper(pt1->retval);
                           }
                           if (localcounter == retvallen) {
                              // flip the buffer back to normal if it was
                              // reversed in the first place
                              if (pt1->endofstring) {
                                 string_flipper(printbuffer);
                              }
                              pt1->print = true;
                           }
                        } else {
                           pt1->print = true;
                        }
                        // if print true set then print line and increment
                        if (pt1->print) {
                           printf("%s\n", 
                                  printbuffer);
                           pcounter += 1;
                           pt1->print = false;
                        }
                     }
                     // clean up local buffers
                     free(printbuffer); printbuffer = NULL;
                  }
                  // clean up file content buffer
                  free(filereadbuffer); filereadbuffer = NULL;
                  free(localbuf); localbuf = NULL;
               }
               //free(localbuf); localbuf = NULL;
            }
         }
         // clean up file pointer
         fclose(filetoread); filetoread = NULL;
      }
   }
   // set package count for return to main and close directory
   pt1->packagecount = pcounter;
   closedir(dir);
}

