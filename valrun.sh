#!/bin/sh

VALGRINDCMD='valgrind -s --leak-check=full --track-origins=yes'
make clean
make

case "$1" in
   full)
      bash -c "${VALGRINDCMD} ./aptsort --arch amd64 --pattern test$"
      sleep 5
      bash -c "${VALGRINDCMD} ./aptsort --arch amd64 --pattern ^test"
      sleep 5
      bash -c "${VALGRINDCMD} ./aptsort --arch amd64 --pattern test"
      sleep 5
      ;;
   mp1)
      bash -c "${VALGRINDCMD} ./aptsort --arch --pattern lol"
      sleep 5
      ;;
   mp2)
      bash -c "${VALGRINDCMD} ./aptsort --arch amd64 --pattern"
      sleep 5
      ;;
   noarg)
      bash -c "${VALGRINDCMD} ./aptsort --arch --pattern"
      sleep 5
      ;;
esac
